<?php
/**
 * Dasboard - Implements a simple Administration Dashboard.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */

//namespace App\Modules\System\Controllers\Admin;
namespace App\Modules\EmployeeArea\Controllers\Ea;

use App\Modules\EmployeeArea\Controllers\EaController;

use View;


class Dashboard extends EaController
{

    public function index()
    {
        // Vamos definir o layout = 'BackendEa'.
        //Clone do C:\xampp\htdocs\w.teclativa.com\app\Themes\AdminLite\Layouts\BackendEa.php
        //com $user = AuthCa::user();

        $this->layout = 'BackendEa';

        return $this->getView()
            ->shares('title', __d('system', 'Dashboard'));
    }

}
