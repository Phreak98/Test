<?php

namespace App\Modules\EmployeeArea\AuthEa\Reminders;

use App\Modules\EmployeeArea\AuthEa\Console\ClearRemindersCommand;
use Nova\Support\ServiceProvider;


class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('command.authea.reminders.clear', function()
        {
            return new ClearRemindersCommand;
        });

        $this->commands('command.authea.reminders.clear');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('command.authea.reminders.clear');
    }

}
