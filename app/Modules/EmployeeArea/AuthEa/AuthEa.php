<?php

namespace App\Modules\EmployeeArea\AuthEa;

use Nova\Support\Facades\Facade;


/**
 * @see \Nova\AuthCa\AuthManager
 * @see \Nova\AuthCa\Guard
 */
class AuthEa extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'authea'; }
}
