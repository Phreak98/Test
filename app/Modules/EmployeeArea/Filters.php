<?php
/**
 * Routing Filters - all Module's specific Routing Filters are defined here.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */


/** Define Route Filters. */


// Role-based Authorization Filter.
Route::filter('roles', function($route, $request, $value)
{
    $user = AuthEa::user();

    // Explode the passed value on array of accepted User Roles.
    $roles = explode(';', $value);

    if (! $user->hasRole($roles)) {
        $status = __('You are not authorized to access this resource.');

        return Redirect::to('ea/dashboard')->withStatus($status, 'warning');
    }
});


// NOSSOS FILTROS para ea

// Authentication Filters.
Route::filter('authea', function($route, $request) {
    if (AuthEa::check()) {
        //
    }

    // User is not authenticated.
    else if (! $request->ajax()) {
        return Redirect::guest('ea/login');
    } else {
        return Response::make('Unauthorized Access', 403);
    }
});


Route::filter('guestea', function($route, $request) {
    if (AuthEa::guest()) {
        //
    }
    // User is authenticated.
    else if (! $request->ajax()) {
        return Redirect::to('ea/dashboard');
    } else {
        return Response::make('Unauthorized Access', 403);
    }
});
