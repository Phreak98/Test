<?php
/**
 * Routes - all Module's specific Routes are defined here.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */


// The Adminstration Routes.
Route::group(array('prefix' => 'ea', 'namespace' => 'ea'), function()
{
    /** Define static routes. */

    // The Framework's Language Changer.
    Route::get('language/{code}', array('before' => 'referer', 'uses' => 'Language@change'));

    // The CRON runner.
    Route::get('cron/{token}', array('uses' => 'CronRunner@index'));

    // The default Auth Routes.
    Route::get( 'login',  array('before' => 'guestea',      'uses' => 'Authorize@login'));
    Route::post('login',  array('before' => 'guestea|csrf', 'uses' => 'Authorize@postLogin'));
    Route::get( 'logout', array('before' => 'authea',       'uses' => 'Authorize@logout'));
/*
    // The Password Remind.
    Route::get( 'password/remind', array('before' => 'guestea',      'uses' => 'Authorize@remind'));
    Route::post('password/remind', array('before' => 'guest|csrf', 'uses' => 'Authorize@postRemind'));

    // The Password Reset.
    Route::get( 'password/reset/{token}', array('before' => 'guestea',      'uses' => 'Authorize@reset'));
    Route::post('password/reset',         array('before' => 'guestea|csrf', 'uses' => 'Authorize@postReset'));

    // The Account Registration.
    Route::get( 'register',                 array('before' => 'guestea',      'uses' => 'Registrar@create'));
    Route::post('register',                 array('before' => 'guestea|csrf', 'uses' => 'Registrar@store'));
    Route::get( 'register/verify/{token?}', array('before' => 'guestea',      'uses' => 'Registrar@verify'));
    Route::get( 'register/status',          array('before' => 'guestea',      'uses' => 'Registrar@status'));
*/



    // The User's Dashboard.
    Route::get('/',         array('before' => 'authea', 'uses' => 'Dashboard@index'));
    Route::get('dashboard', array('before' => 'authea', 'uses' => 'Dashboard@index'));

    // The User's Profile.
    Route::get( 'profile', array('before' => 'authea',      'uses' => 'Profile@index'));
    Route::post('profile', array('before' => 'authea|csrf', 'uses' => 'Profile@update'));

    // The Site Settings.
    Route::get( 'settings', array('before' => 'authea',      'uses' => 'Settings@index'));
    Route::post('settings', array('before' => 'authea|csrf', 'uses' => 'Settings@store'));

    
});
