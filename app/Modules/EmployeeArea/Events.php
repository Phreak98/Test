<?php
/**
 * Events - all Module's specific Events are defined here.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */


/** Define Events. */

Event::listen('backend_ea.menu', function($user) {
    $items = array(
        array(
            'uri'    => 'ea/dashboard',
            'title'  => __d('system', 'Dashboard'),
            'icon'   => 'dashboard',
            'weight' => 0,
        )
    );


    $items[] = array(
        'uri'    => 'ea/profile',
        'title'  => __d('system', 'Profile'),
        'icon'   => 'gears',
        'weight' => 0,
    );


    if ($user->hasRole('administrator')) {
        $items[] = array(
            'uri'    => 'ea/settings',
            'title'  => __d('system', 'Settings'),
            'icon'   => 'gears',
            'weight' => 0,
        );
    }

    return $items;
});
