<?php
/**
 * Users - A Controller for managing the Users Authentication.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */

namespace App\Modules\ClientArea\Controllers\Ca;

use App\Modules\ClientArea\Controllers\CaController;
use App\Models\User;

//use Auth;
use App\Modules\ClientArea\AuthCa\AuthCa as Auth;
use Hash;
use Input;
use Redirect;
use Validator;
use View;


class Profile extends CaController
{

    protected function validate(array $data, User $user)
    {
        // Prepare the Validation Rules, Messages and Attributes.
        $rules = array(
            'current_password'      => 'required|valid_password',
            'password'              => 'required|strong_password',
            'password_confirmation' => 'required|same:password',
        );

        $messages = array(
            'valid_password'  => __d('system', 'The :attribute field is invalid.'),
            'strong_password' => __d('system', 'The :attribute field is not strong enough.'),
        );

        $attributes = array(
            'current_password'      => __d('system', 'Current Password'),
            'password'              => __d('system', 'New Password'),
            'password_confirmation' => __d('system', 'Password Confirmation'),
        );

        // Add the custom Validation Rule commands.
        Validator::extend('valid_password', function($attribute, $value, $parameters) use ($user)
        {
            return Hash::check($value, $user->password);
        });

        Validator::extend('strong_password', function($attribute, $value, $parameters)
        {
            $pattern = "/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/";

            return (preg_match($pattern, $value) === 1);
        });

        return Validator::make($data, $rules, $messages, $attributes);
    }

    public function index()
    {
        // Vamos definir o layout = 'BackendCa'.
        //Clone do C:\xampp\htdocs\w.teclativa.com\app\Themes\AdminLite\Layouts\BackendCa.php
        //com $user = AuthCa::user();

        $this->layout = 'BackendCa';
        
        $user = Auth::user();

        return $this->getView()
            ->shares('title',  __d('system', 'User Profile'))
            ->with('user', $user);
    }

    public function update()
    {
        // Vamos definir o layout = 'BackendCa'.
        //Clone do C:\xampp\htdocs\w.teclativa.com\app\Themes\AdminLite\Layouts\BackendCa.php
        //com $user = AuthCa::user();

        $this->layout = 'BackendCa';

        $user = Auth::user();

        // Retrieve the Input data.
        $input = Input::only('current_password', 'password', 'password_confirmation');

        // Create a Validator instance.
        $validator = $this->validate($input, $user);

        // Validate the Input.
        if ($validator->passes()) {
            $password = $input['password'];

            // Update the password on the User Model instance.
            $user->password = Hash::make($password);

            // Save the User Model instance.
            $user->save();

            // Use a Redirect to avoid the reposting the data.
            $status = __d('system', 'You have successfully updated your Password.');

            return Redirect::back()->withStatus($status);
        }

        // Collect the Validation errors.
        $status = $validator->errors()->all();

        return Redirect::back()->withStatus($status, 'danger');
    }

}
