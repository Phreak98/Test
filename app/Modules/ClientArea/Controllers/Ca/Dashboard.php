<?php
/**
 * Dasboard - Implements a simple Administration Dashboard.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */

//namespace App\Modules\System\Controllers\Admin;
namespace App\Modules\ClientArea\Controllers\Ca;


use App\Modules\ClientArea\Controllers\CaController;

use View;


class Dashboard extends CaController
{

    public function index()
    {
        // Vamos definir o layout = 'BackendCa'.
        //Clone do C:\xampp\htdocs\w.teclativa.com\app\Themes\AdminLite\Layouts\BackendCa.php
        //com $user = AuthCa::user();

        return $this->getView()
            ->shares('title', __d('system', 'Dashboard'));
    }

}
