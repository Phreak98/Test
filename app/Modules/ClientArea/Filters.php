<?php
/**
 * Routing Filters - all Module's specific Routing Filters are defined here.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */


/** Define Route Filters. */


// Role-based Authorization Filter.
Route::filter('roles', function($route, $request, $value)
{
    $user = AuthCa::user();

    // Explode the passed value on array of accepted User Roles.
    $roles = explode(';', $value);

    if (! $user->hasRole($roles)) {
        $status = __('You are not authorized to access this resource.');

        return Redirect::to('ca/dashboard')->withStatus($status, 'warning');
    }
});


// NOSSOS FILTROS para ca

// Authentication Filters.
Route::filter('auth_ca', function($route, $request) {
    if (AuthCa::check()) {
        //
    }

    // User is not authenticated.
    else if (! $request->ajax()) {
        return Redirect::guest('ca/login');
    } else {
        return Response::make('Unauthorized Access', 403);
    }
});


Route::filter('guest_ca', function($route, $request) {
    if (AuthCa::guest()) {
        //
    }
    // User is authenticated.
    else if (! $request->ajax()) {
        return Redirect::to('ca/dashboard');
    } else {
        return Response::make('Unauthorized Access', 403);
    }
});
