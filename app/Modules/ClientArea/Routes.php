<?php
/**
 * Routes - all Module's specific Routes are defined here.
 *
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */


// The Adminstration Routes.
Route::group(array('prefix' => 'ca', 'namespace' => 'ca'), function()
{
    /** Define static routes. */

    // The Framework's Language Changer.
    Route::get('language/{code}', array('before' => 'referer', 'uses' => 'Language@change'));

    // The CRON runner.
    Route::get('cron/{token}', array('uses' => 'CronRunner@index'));

    // The default Auth Routes.
    Route::get( 'login',  array('before' => 'guest_ca',      'uses' => 'Authorize@login'));
    Route::post('login',  array('before' => 'guest_ca|csrf', 'uses' => 'Authorize@postLogin'));
    Route::get( 'logout', array('before' => 'auth_ca',       'uses' => 'Authorize@logout'));
/*
    // The Password Remind.
    Route::get( 'password/remind', array('before' => 'guest_ca',      'uses' => 'Authorize@remind'));
    Route::post('password/remind', array('before' => 'guest|csrf', 'uses' => 'Authorize@postRemind'));

    // The Password Reset.
    Route::get( 'password/reset/{token}', array('before' => 'guest_ca',      'uses' => 'Authorize@reset'));
    Route::post('password/reset',         array('before' => 'guest_ca|csrf', 'uses' => 'Authorize@postReset'));

    // The Account Registration.
    Route::get( 'register',                 array('before' => 'guest_ca',      'uses' => 'Registrar@create'));
    Route::post('register',                 array('before' => 'guest_ca|csrf', 'uses' => 'Registrar@store'));
    Route::get( 'register/verify/{token?}', array('before' => 'guest_ca',      'uses' => 'Registrar@verify'));
    Route::get( 'register/status',          array('before' => 'guest_ca',      'uses' => 'Registrar@status'));
*/



    // The User's Dashboard.
    Route::get('/',         array('before' => 'auth_ca', 'uses' => 'Dashboard@index'));
    Route::get('dashboard', array('before' => 'auth_ca', 'uses' => 'Dashboard@index'));

    // The User's Profile.
    Route::get( 'profile', array('before' => 'auth_ca',      'uses' => 'Profile@index'));
    Route::post('profile', array('before' => 'auth_ca|csrf', 'uses' => 'Profile@update'));

    // The Site Settings.
    Route::get( 'settings', array('before' => 'auth_ca',      'uses' => 'Settings@index'));
    Route::post('settings', array('before' => 'auth_ca|csrf', 'uses' => 'Settings@store'));

    
});
