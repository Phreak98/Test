<?php

namespace App\Modules\ClientArea\AuthCa;

use Nova\Support\Facades\Facade;


/**
 * @see \Nova\AuthCa\AuthManager
 * @see \Nova\AuthCa\Guard
 */
class AuthCa extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'authca'; }
}
