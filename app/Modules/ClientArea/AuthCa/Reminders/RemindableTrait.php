<?php

namespace App\Modules\ClientArea\AuthCa\Reminders;


trait RemindableTrait
{
    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

}
