<?php

namespace App\Modules\ClientArea\AuthCa\Reminders;

use App\Modules\ClientArea\AuthCa\Console\ClearRemindersCommand;
use Nova\Support\ServiceProvider;


class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('command.authca.reminders.clear', function()
        {
            return new ClearRemindersCommand;
        });

        $this->commands('command.authca.reminders.clear');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('command.authca.reminders.clear');
    }

}
